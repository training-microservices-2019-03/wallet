create table wallet (
    id varchar (36),
    id_user varchar (36) not null,
    name varchar (100) not null,
    primary key (id),
    unique (id_user, name)
);

create table wallet_transaction (
    id varchar (36),
    id_wallet varchar (36) not null,
    transaction_time timestamp not null,
    amount decimal(19,2) not null,
    description varchar (255) not null,
    reference varchar(40),
    primary key (id),
    foreign key (id_wallet) references wallet(id)
);