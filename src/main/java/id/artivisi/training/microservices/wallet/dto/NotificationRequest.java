package id.artivisi.training.microservices.wallet.dto;

import lombok.Builder;
import lombok.Data;

@Data @Builder
public class NotificationRequest {
    private String noHp;
    private String email;
    private String message;
    private String title;
}
