package id.artivisi.training.microservices.wallet.controller;

import id.artivisi.training.microservices.wallet.dao.WalletTransactionDao;
import id.artivisi.training.microservices.wallet.entity.Wallet;
import id.artivisi.training.microservices.wallet.entity.WalletTransaction;
import id.artivisi.training.microservices.wallet.service.KafkaSenderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

@RestController
public class WalletTransactionController {

    private static final Logger LOGGER = LoggerFactory.getLogger(WalletTransactionController.class);

    @Autowired private WalletTransactionDao walletTransactionDao;
    @Autowired private KafkaSenderService kafkaSenderService;

    @GetMapping("/wallet/{id}/transaction")
    public Iterable<WalletTransaction> findByWallet(@PathVariable("id") Wallet wallet){
        LOGGER.info("Mencari transaksi untuk wallet {} milik user {}", wallet.getName(), wallet.getIdUser());
        return walletTransactionDao.findByWalletOrderByTransactionTime(wallet);
    }

    //@PreAuthorize("hasAuthority('edit-transaksi')")
    @GetMapping("/hostinfo")
    public Map<String, Object> hostInfo(HttpServletRequest request) throws UnknownHostException {
        Map<String, Object> info = new HashMap<>();
        info.put("hostname", InetAddress.getLocalHost().getHostName());
        info.put("address", request.getLocalAddr());
        info.put("port", request.getLocalPort());
        return info;
    }

    @PostMapping("/wallet/{id}/transaction")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@PathVariable("id") Wallet wallet,
                                  @RequestBody @Valid WalletTransaction wt) {
        walletTransactionDao.save(wt);
        kafkaSenderService.kirimNotifikasi(wallet.getIdUser()+"@example.com",
                "08120000", "Sukses update saldo",
                "Saldo anda berubah sebanyak "+wt.getAmount().toPlainString());
    }

}
