package id.artivisi.training.microservices.wallet.dao;

import id.artivisi.training.microservices.wallet.entity.Wallet;
import id.artivisi.training.microservices.wallet.entity.WalletTransaction;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface WalletTransactionDao extends PagingAndSortingRepository<WalletTransaction, String> {
    Iterable<WalletTransaction> findByWalletOrderByTransactionTime(Wallet wallet);
}
