package id.artivisi.training.microservices.wallet.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.artivisi.training.microservices.wallet.dto.NotificationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaSenderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaSenderService.class);

    @Value("${kafka.topic.notification}")
    private String topicNotification;

    @Autowired private KafkaTemplate<String, String> kafkaTemplate;
    @Autowired private ObjectMapper objectMapper;

    public void kirimNotifikasi(String email, String hp, String title, String message) {
        NotificationRequest nr = NotificationRequest.builder()
                .email(email)
                .noHp(hp)
                .title(title)
                .message(message)
                .build();

        String kafkaMsg = null;
        try {
            kafkaMsg = objectMapper.writeValueAsString(nr);
            LOGGER.debug("Kirim notifikasi : {}", kafkaMsg);
            kafkaTemplate.send(topicNotification, kafkaMsg);
        } catch (JsonProcessingException e) {
            LOGGER.error(e.getMessage(), e);
        }

    }
}
