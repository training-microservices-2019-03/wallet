package id.artivisi.training.microservices.wallet.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity @Data
public class WalletTransaction {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_wallet")
    private Wallet wallet;

    @NotNull
    private LocalDateTime transactionTime = LocalDateTime.now();

    @NotNull
    private BigDecimal amount;

    @NotEmpty @Size(min = 5, max = 255)
    private String description;

    private String reference;
}
